var data = {
  set: function(key, value) {
    if (!key || !value) {return;}

    if (typeof value === "object") {
      value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
  },
  get: function(key) {
    var value = localStorage.getItem(key);

    if (!value) { return; }

    if (value[0] === "{") {
      value = JSON.parse(value);
    }

    return value;
  }
}

let app = new Vue({
  el: '#container',
  data: function () {
    return {
      account: data.get("account"),
      entry: data.get("entry"),
      exit: data.get("exit"),
      stopLoss: data.get("stopLoss"),
      percentRisk: data.  get("percentRisk"),
      entries: data.get("entries"),
      leverage: data.get("leverage"),
      xbtusd: null,
    }
  },
  computed: {
    isLong: function() {
      var position = null;
      if (this.exit && this.entry) {
        if (this.exit > this.entry) {
          position = 'long';
        }
        if (this.entry > this.exit) {
          position = 'short';
        }
      }
      return position;
    },
    usdAccount: function() {
      return (this.xbtusd * this.account).toFixed(0);
    },
    accountRisk: function() {
      if (this.entry && this.exit && this.stopLoss && this.percentRisk && this.account) {
        return ((this.percentRisk/100) * this.usdAccount).toFixed(0);
      }
    },
    rrRatio: function () {
      if (this.exit && this.entry && this.stopLoss)
          return ((this.exit - this.entry) / (this.entry - this.stopLoss)).toFixed(2);
    },
    risk: function () {
      if (this.exit && this.entry && this.stopLoss)
        return Math.abs( ((this.entry - this.stopLoss) / this.entry * 100) ).toFixed(1);
    },
    reward: function () {
      if (this.exit && this.entry && this.stopLoss)
        return Math.abs(((this.exit - this.entry) / this.entry * 100)).toFixed(1);
    },
    positionSize: function () {
      if (this.account && this.percentRisk && this.exit && this.entry && this.stopLoss)
        return ((this.usdAccount * (this.percentRisk/100)) / (this.risk/100)).toFixed(0);
    },
    average: function () {
      if (this.entries) {
          var entries = [];

            this.entries.replace(/([\d\.]+)[\D@\D]+([\d\.]+)/g,
            function(pattern, contracts, price) {
              entries.push({
                "contracts": Number(contracts),
                "price": Number(price)
              });
            }
          );

          var sum = 0;
          var totalContracts = 0;

          entries.forEach(e => {
            var contracts = e["contracts"];
            var price = e["price"];

            sum += (contracts * price);
            totalContracts += contracts;
          })

          return (sum / totalContracts).toFixed(2);
      }
    }
  }
});

function saveTrade() {
  if (app.entry && app.exit && app.stopLoss) {
    var trades = data.get('names');
    if (!trades) {
      trades = [];
    }
    var saved = {
      entry: app.entry,
      exit: app.exit,
      stopLoss: app.stopLoss
    }

    data.set('saved',)
  }
}

let mexSocket = new WebSocket("wss://www.bitmex.com/realtime?subscribe=trade:XBTUSD");

mexSocket.onmessage = e => {
  var msg = JSON.parse(e.data);
  if (msg.table == "trade")
    app.xbtusd = msg.data[0].price;
    document.title = '$' + app.xbtusd;
}

function getLocalData (name) {
  return localStorage.getItem(name);
}

function setLocalData (name, value) {
  localStorage.setItem(name, value);
}
